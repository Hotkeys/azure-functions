const _ = require('underscore');
exports.addMissingTimeToWord = function (captionTrack) {
    return _.map(captionTrack, function (item) {
        if ((_.last(item.words)).time === undefined) {
            var lastIndex = item.words.length - 1;
            item.words[lastIndex].time = item.words[lastIndex - 1].time + 250;
        }
        return item;
    });
}

exports.removeDuplicateStoriesByTime = function (stories) {
    return _.uniq(stories, function (item, key, hotkeyWordTriggerTime) {
        return item.hotkeyWordTriggerTime;
    });
}

exports.returnArrayByKeyValues = function (list, key) {
    return _.map(list, function (item) {
        return item[key];
    });
}