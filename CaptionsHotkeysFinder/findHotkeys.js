const Fuse = require('fuse.js');
const _ = require('underscore');
const constants = require('../ConfigsAndSharedLibraries/constants');
const helper = require('./helper');
const didYouMean = require('didyoumean2');

module.exports = function (captionTrack, clipType, hotkeyWords) {
    const getHighlight = require('./getHighlight');
    const getStory = require('./getStory');

    captionTrack = helper.addMissingTimeToWord(captionTrack);

    let hotkeyConstants = '';
    switch (clipType) {
        case constants.HOTKEY.CLIPTYPE.HK15:
            hotkeyConstants = constants.HOTKEY.HK15;
            break;
        case constants.HOTKEY.CLIPTYPE.HK30:
            hotkeyConstants = constants.HOTKEY.HK30;
            break;
        case constants.HOTKEY.CLIPTYPE.HK60:
            hotkeyConstants = constants.HOTKEY.HK60;
            break;
    }
    hotkeyWords = (hotkeyWords === undefined) ? hotkeyConstants.DEFAULT_WORDS : hotkeyWords;
    let hotkeyStoriesList = [];
    _.each(hotkeyWords, (hotkeyWord) => {
        let options;
        switch (hotkeyWord.searchType) {
            case 'exact':
                options = require('./fuseOptions/exact')();
                break;
            case 'fuzzy':
                options = require('./fuseOptions/fuzzy')();
                break;
        }
        let fuse = new Fuse(captionTrack, options);
        let results = fuse.search(hotkeyWord.word);
        _.each(results, (item) => {
            let wordsList = item.item.words,
                wordsOnlyList = helper.returnArrayByKeyValues(wordsList, 'word');
            let levenshteinWordMatchFromList = didYouMean(hotkeyWord.word, wordsOnlyList);
            let wordMatchItem = _.findWhere(wordsList, { word: levenshteinWordMatchFromList });

            hotkeyStoriesList.push(
                _.extend({ hotkeyType: hotkeyConstants.TYPE },
                    getHighlight(wordMatchItem, hotkeyConstants),
                    getStory(captionTrack, wordMatchItem, hotkeyConstants)
                )
            )
        });
    })
    hotkeyStoriesList = helper.removeDuplicateStoriesByTime(hotkeyStoriesList);
    return hotkeyStoriesList;
}