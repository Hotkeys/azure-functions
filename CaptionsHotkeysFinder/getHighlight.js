const _ = require('underscore');
module.exports = function getHotkeyHighlight(hotkeyWordItem, hotkeyConstants) {
    return {
        hotkeyWordTriggerTime: hotkeyWordItem.time,
        sceneDuration: hotkeyConstants.CLIPLENGTH,
    }
}