const _ = require('underscore');

module.exports = function getHotkeyStory(resultList, hotkeyWordItem, hotkeyConstants) {
    var listOfWordItems = _.flatten(_.map(resultList, function (item) { return item.words; }))
    var hotkeyWordItemIndex = listOfWordItems.findIndex((element) => (element == hotkeyWordItem));
    let hasDeadAir = (nextWordTime, currentWordTime) => (nextWordTime - currentWordTime) >= hotkeyConstants.DEAD_AIRTIME;
    let hasExceededHotkeyStoryMaxTime = (currTime, startTime) => (currTime - startTime) > hotkeyConstants.MAX_STORYTIME;

    var hotkeyTitleAndDescription = function (start_item, wordsList, start_index) {
        //add words till deadair is found between to words
        //or add words till storyLength is passed.
        let i = start_index;
        let end_time;
        let storyWordsList = [];

        while (hasDeadAir(wordsList[i + 1].time, wordsList[i].time) === false
            && hasExceededHotkeyStoryMaxTime(wordsList[i + 1].time, start_item.time) === false) {
            end_time = wordsList[i].time;
            i += 1;
            storyWordsList.push(wordsList[i].word);
        }
        return {
            text: storyWordsList.join(' '),
            storyGiven: storyWordsList.length > 0,
            begin: start_item.time,
            end: end_time
        }
    }

    let nextWordToHotkeyItem = listOfWordItems[hotkeyWordItemIndex + 1];
    let hkStory = { storyGiven: false, text: '', begin: 0, end: 0 };
    if (nextWordToHotkeyItem && hasDeadAir(hotkeyWordItemIndex, nextWordToHotkeyItem) === false) {
        hkStory = hotkeyTitleAndDescription(hotkeyWordItem, listOfWordItems, hotkeyWordItemIndex);
    }
    return {
        hasHotkeyStory: hkStory.storyGiven,
        hotkeyStoryText: hkStory.text,
        hotkeyStoryBeginTime: hkStory.begin,
        hotkeyStoryEndTime: hkStory.end
    }
}