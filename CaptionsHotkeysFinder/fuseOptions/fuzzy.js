module.exports = function () {
    return {
        shouldSort: true,
        tokenize: true,
        matchAllTokens: true,
        includeScore: true,
        includeMatches: true,
        threshold: 0.3,
        location: 0,
        distance: 100,
        maxPatternLength: 32,
        minMatchCharLength: 1,
        keys: [
            "part"
        ]
    }
}