module.exports = function () {
    return {
        caseSensitive: true,
        shouldSort: true,
        tokenize: true,
        findAllMatches: true,
        includeScore: true,
        includeMatches: true,
        threshold: 0,
        location: 0,
        distance: 100,
        maxPatternLength: 32,
        minMatchCharLength: 2,
        keys: [
            "part",
        ]
    }
}