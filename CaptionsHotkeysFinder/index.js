const aS = require('../ConfigsAndSharedLibraries/azureStorage');
const postgres = require('../ConfigsAndSharedLibraries/azurePostgres');
var queue = aS.createServicePromiseWrapper('queue');
var table = aS.createServicePromiseWrapper('table');
var entGen = aS.entGen;
var findHotkeys = require('./findHotkeys');
const hotkeyConstants = require('../ConfigsAndSharedLibraries/constants').HOTKEY;

const _ = require('underscore');
function findHotkeysInCaptionTrack(captionTrack) {
    captionTrack = _.extend({}, captionTrack);
    var hotkeyStoriesAndHighlights = [].concat(
        findHotkeys(captionTrack, hotkeyConstants.CLIPTYPE.HK15, undefined),
        findHotkeys(captionTrack, hotkeyConstants.CLIPTYPE.HK30, undefined),
        findHotkeys(captionTrack, hotkeyConstants.CLIPTYPE.HK60, undefined)
    )
    hotkeyStoriesAndHighlights = _.sortBy(hotkeyStoriesAndHighlights, function (o) { return o.hotkeyWordTriggerTime; })
    return hotkeyStoriesAndHighlights;
}
module.exports = function (context, myQueueItem) {
    myQueueItem = '{"channel_id":"UCzhF4pw0Yw3EP1aOZcikoVQ","video_id":"2z-5cQdTpbY"}';
    let messageObj = (typeof myQueueItem === 'string') ? JSON.parse(myQueueItem) : myQueueItem;
    let channel_id = messageObj.channel_id;
    let video_id = messageObj.video_id;

    let EntityID = { tableName: 'publishedVideos', pK: channel_id, rK: video_id };
    table.x('retrieveEntity', EntityID.tableName, EntityID.pK, EntityID.rK)
        .then((tableData) => {
            let captionsTrack = JSON.parse(tableData.captionsTrack._);
            return new Promise((resolve, reject) => {
                //process hotkeys
                resolve(findHotkeysInCaptionTrack(captionsTrack));
            })
                .then((hotkeysTimeFramesList) => table.x('insertOrMergeEntity', EntityID.tableName, {
                    PartitionKey: entGen.String(EntityID.pK),
                    RowKey: entGen.String(EntityID.rK),
                    captionsHotkeysFound: entGen.Boolean(hotkeysTimeFramesList.length > 0),
                    captionsHotkeysTimeFrames: entGen.String(JSON.stringify(hotkeysTimeFramesList))
                }))
                .then(() => queue.x('createMessage', 'youtube-video-download',
                    new Buffer(JSON.stringify({
                        "channel_id": channel_id,
                        "video_id": video_id
                    })).toString('base64')
                ))
                .then(() => {
                    context.done();
                });
        })

}
