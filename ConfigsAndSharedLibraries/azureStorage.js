var pub = {};
var azure = require('azure-storage');
pub.entGen = azure.TableUtilities.entityGenerator;
var tableService = azure.createTableService();
var queueService = azure.createQueueService();

function serviceInstance(serviceName) {
    var service;

    switch (serviceName.toLowerCase()) {
        case 'queue':
            service = azure.createQueueService();
            break;
        case 'table':
            service = azure.createTableService();
            break;
        case 'blob':
            service = azure.createBlobService();
            break;
        case 'file':
            service = azure.createFileService();
            break;
    }
    return {
        x: function () {
            var _resolve, _reject;
            var argumentsArray = Array.prototype.slice.call(arguments);
            var methodType = argumentsArray.shift();
            argumentsArray.push(function (error, result) {
                if (error) _reject(error);
                _resolve(result || true);
            });
            return new Promise(function (resolve, reject) {
                _resolve = resolve;
                _reject = reject;
                return service[methodType].apply(service, argumentsArray)
            });
        }
    }
}
pub.createServicePromiseWrapper = function (serviceName) {
    return new serviceInstance(serviceName);
}

module.exports = pub;