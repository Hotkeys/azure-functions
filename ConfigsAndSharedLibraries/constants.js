module.exports = {
    HOTKEY: {
        CLIPTYPE: {
            HK15: 'HK15',
            HK30: 'HK30',
            HK60: 'HK60'
        },
        'HK15': {
            DEFAULT_WORDS: [
                { word: 'CK', searchType: 'exact' }
            ],
            TYPE: 'HK15',
            CLIPLENGTH: 15000,
            DEAD_AIRTIME: 2500,
            MAX_STORYTIME: 7000
        },
        'HK30': {
            DEFAULT_WORDS: [
                { word: 'HK', searchType: 'exact' }
            ],
            TYPE: 'HK30',
            CLIPLENGTH: 30000,
            DEAD_AIRTIME: 2500,
            MAX_STORYTIME: 8000
        },
        'HK60': {
            DEFAULT_WORDS: [
                { word: 'hotkeys', searchType: 'fuzzy' },
                { word: 'hotkey', searchType: 'fuzzy' }
            ],
            TYPE: 'HK60',
            CLIPLENGTH: 60000,
            DEAD_AIRTIME: 2500,
            MAX_STORYTIME: 15000
        }
    }
}