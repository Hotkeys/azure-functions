const pgp = require('pg-promise')();
/*
const cn = {
    host: 'hotkeys.postgres.database.azure.com',
    port: 5432,
    database: 'hotkeys_dev',
    user: 'sam_hotkeys@hotkeys',
    password: 'Zidane10'
};
*/
const _HOTKEYS_DEV_POSTGRESQL = process.env['HOTKEYS_DEV_POSTGRESQL'];
const db = pgp(_HOTKEYS_DEV_POSTGRESQL);

function getUserByYoutubeChannelID(channel_id) {
    return new Promise((resolve, reject) => {
        db.func('users_get_user_by_youtube_channel_id', [channel_id])
            .then(data => {
                resolve(data[0]);
            })
            .catch(error => {
                console.log(error); // printing the error
            });
    })
}

module.exports = {
    getUserByYoutubeChannelID: getUserByYoutubeChannelID
}



