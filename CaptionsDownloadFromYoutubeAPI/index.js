const aS = require('../ConfigsAndSharedLibraries/azureStorage');
const postgres = require('../ConfigsAndSharedLibraries/azurePostgres');
var queue = aS.createServicePromiseWrapper('queue');
var table = aS.createServicePromiseWrapper('table');
var entGen = aS.entGen;
var google = require('../GoogleAPITokenCheck/googleAPIAuthSetup');

module.exports = function (context, myQueueItem) {
    let messageObj = (typeof myQueueItem === 'string') ? JSON.parse(myQueueItem) : myQueueItem;
    let channel_id = messageObj.channel_id;
    let video_id = messageObj.video_id;
    let EntityID = { tableName: 'publishedVideos', pK: channel_id, rK: video_id };
    google.checkTokenAndSetCredentials(messageObj.refreshToken, messageObj.token, messageObj.tokenExpiryDate)
        .then((data) => table.x('retrieveEntity', EntityID.tableName, EntityID.pK, EntityID.rK)
            .then((tableData) => {
                let captionsEnglishSnippet = JSON.parse(tableData.captionsEnglishSnippet);
                return new Promise((resolve, reject) => {
                    google.apis.youtube('v3')
                        .captions.download({
                            auth: google.oauth2Client,
                            id: captionsEnglishSnippet.id,
                            tfmt: 'vtt'
                        }, function (err, response) {
                            if (err) { context.log('The API returned an error: ' + err); return; }
                            resolve(response.items);
                        });
                })
                    .then((vttData) => vttToJSON(response))
                    .then((captionsTrackData) => table.x('insertOrMergeEntity', EntityID.tableName, {
                        PartitionKey: entGen.String(EntityID.pK),
                        RowKey: entGen.String(EntityID.rK),
                        captionsTrack: entGen.String(JSON.stringify(captionsTrackData))
                    }))
                    .then(() => queue.x('createMessage', 'hotkeys-captions-check',
                        new Buffer(JSON.stringify({
                            "channel_id": channel_id,
                            "video_id": video_id,
                        })).toString('base64')
                    ))
                    .then(() => {
                        context.done();
                    });
            })
        )
}
