'use strict';
const CLIENT_ID = process.env['GOOGLE_API_CLIENT_ID'];
const CLIENT_SECRET = process.env['GOOGLE_API_CLIENT_SECRET'];

const google = require('googleapis');
const OAuth2 = google.auth.OAuth2;
const oauth2Client = new OAuth2(CLIENT_ID, CLIENT_SECRET);

var pub = {};
var internals = {};
internals.isTokenExpired = function (expiry_date = 0) {
    var now = new Date();
    console.log('is expired: ', now.getTime() > expiry_date);
    console.log(now.getTime(), ' = ', expiry_date);
    return now.getTime() > expiry_date;
}

pub.checkTokenAndSetCredentials = function (refreshToken, token = false, expiryDate = 0) {
    return new Promise((resolve, reject) => {
        if (internals.isTokenExpired(expiryDate)) {
            oauth2Client.setCredentials({
                refresh_token: refreshToken
            });
            oauth2Client.refreshAccessToken(function (err, tokens) {
                console.log(tokens);
                resolve(tokens);
            });
        } else {
            resolve({
                access_token: token,
                refresh_token: refreshToken
            });
        }
    })
};

pub.isTokenExpired = internals.isTokenExpired;
pub.apis = google;
pub.oauth2Client = oauth2Client;
pub.getNewToken = function (refresh_token) {
    return new Promise(function (resolve, reject) {
        oauth2Client.setCredentials({
            refresh_token: refresh_token
        });
        oauth2Client.refreshAccessToken(function (err, tokens) {
            resolve(tokens);
        });
    });
}

module.exports = pub;