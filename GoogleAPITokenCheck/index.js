const aS = require('../ConfigsAndSharedLibraries/azureStorage');
const postgres = require('../ConfigsAndSharedLibraries/azurePostgres');
var queue = aS.createServicePromiseWrapper('queue');
var table = aS.createServicePromiseWrapper('table');
var entGen = aS.entGen;

const moment = require('moment');

module.exports = function (context, myQueueItem) {
    var googleAuth = require('./googleAPIAuthSetup');
    let messageObj = (typeof myQueueItem === 'string') ? JSON.parse(myQueueItem) : myQueueItem;
    let channel_id = messageObj.channel_id;
    let video_id = messageObj.video_id;
    let googleAPI_queue = messageObj.googleapi_queue;
    let EntityID = { tableName: 'subscribedChannels', pK: 'channel_id', rK: channel_id };

    table.x('retrieveEntity', EntityID.tableName, EntityID.pK, EntityID.rK).then((tableData) => {
        return new Promise((resolve, reject) => {
            if (googleAuth.isTokenExpired(tableData.tokenExpiryDateUTC._)) {
                postgres.getUserByYoutubeChannelID(channel_id)
                    .then((user) => googleAuth.getNewToken(user.google_refresh_token))
                    .then((tokens) => table.x('insertOrMergeEntity',
                        EntityID.tableName,
                        {
                            PartitionKey: entGen.String(EntityID.pK),
                            RowKey: entGen.String(EntityID.rK),
                            refreshToken: entGen.String(tokens.refresh_token),
                            token: entGen.String(tokens.access_token),
                            tokenExpiryDateUTC: entGen.Int64(tokens.expiry_date),
                            tokenExpiryDate: entGen.DateTime(new Date(tokens.expiry_date))
                        }))
                    .then(() => table.x('retrieveEntity', EntityID.tableName, EntityID.pK, EntityID.rK))
                    .then((updatedTableData) => {
                        context.log('new data');
                        context.log(updatedTableData);
                        resolve({
                            channel_id: channel_id,
                            video_id: video_id,
                            refreshToken: updatedTableData.refreshToken._,
                            token: updatedTableData.token._,
                            tokenRenewed: true,
                            tokenExpiryDateUTC: updatedTableData.tokenExpiryDateUTC._
                        })
                    }, (err) => context.log(err));
            } else {
                context.log('old data');
                context.log(tableData);
                resolve({
                    channel_id: channel_id,
                    video_id: video_id,
                    refreshToken: tableData.refreshToken._,
                    token: tableData.token._,
                    tokenRenewed: false,
                    tokenExpiryDateUTC: tableData.tokenExpiryDateUTC._
                })
            }
        })
            .then(function (messageText) {
                queue.x('createMessage', googleAPI_queue,
                    new Buffer(JSON.stringify(
                        messageText
                    )).toString('base64')).then(() => {
                        context.done();
                    })
            })
    });
}