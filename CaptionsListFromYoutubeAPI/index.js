const aS = require('../ConfigsAndSharedLibraries/azureStorage');
const postgres = require('../ConfigsAndSharedLibraries/azurePostgres');
var queue = aS.createServicePromiseWrapper('queue');
var table = aS.createServicePromiseWrapper('table');
var entGen = aS.entGen;

var google = require('../GoogleAPITokenCheck/googleAPIAuthSetup');
function findCaptionTrackByLanguage(lang, listOfCaptionsTrack) {
    return _.find(listOfCaptionsTrack, function (n) {
        if (n.snippet.language === lang) return n;
    });
}
module.exports = function (context, req) {
    let myQueueItem = '{"channel_id":"UCzhF4pw0Yw3EP1aOZcikoVQ","video_id":"2z-5cQdTpbY","token":"ya29.Gl0cBReOoTzdwl1nc7R4Z3oX7wz3pi3y0F9RcwkPgISQf7j118Liu6Yj1w8DO8eyCRAPMdCCyhvK1jHjzmGVdUesgmrVSlX3xDXB9hAd063eO1gg4jyMb0R3h784Gm4","tokenRenewed":true,"tokenExpiryDateUTC":"1512761147664", "refreshToken":"1/p8mCx9E0OVYgPqH8skuqnlYu6EIYR0CRitdndx2av4M"}';
    let messageObj = (typeof myQueueItem === 'string') ? JSON.parse(myQueueItem) : myQueueItem;

    let channel_id = messageObj.channel_id;
    let video_id = messageObj.video_id;
    let EntityID = { tableName: 'publishedVideos', pK: channel_id, rK: video_id };
    google.checkTokenAndSetCredentials(messageObj.refreshToken, messageObj.token, messageObj.tokenExpiryDateUTC)
        .then((data) => {
            return new Promise((resolve, reject) => {
                google.apis.youtube('v3')
                    .captions.list({
                        auth: google.oauth2Client,
                        part: 'snippet',
                        videoId: video_id
                    }, function (err, response) {
                        if (err) { context.log('The API returned an error: ' + err); return; }
                        resolve(response.items);
                    });
            }).then((captionsListData) => {
                var captionsTracksFound = captionsListData.length > 0;
                const CAPTIONS_LANGUAGE = 'en';
                var captionsEnglishTrack = findCaptionTrackByLanguage(CAPTIONS_LANGUAGE, captionsListData)

                if (captionsEnglishTrack) {
                    table.x('insertOrMergeEntity', EntityID.tableName, {
                        PartitionKey: entGen.String(EntityID.pK),
                        RowKey: entGen.String(EntityID.rK),
                        captionsFound: entGen.Boolean(captionsTracksFound),
                        captionsEnglishFound: entGen.Boolean(_.isObject(captionsEnglishTrack)),
                        captionsEnglishSnippet: entGen.String(JSON.stringify(captionsEnglishTrack)),
                        dateCaptionsFound: entGen.DateTime(new Date()),
                        dateCaptionsEnglishFound: entGen.DateTime(new Date())
                    })
                        .then(() => queue.x('createMessage', 'check-google-api-token',
                            new Buffer(JSON.stringify({
                                "channel_id": channel_id,
                                "video_id": video_id,
                                "googleapi_queue": "get-youtube-api-captions-download"
                            })).toString('base64')))
                        .then(() => {
                            context.done();
                        });
                } else {
                    context.done('Failed to retrieve english closed captions');
                }
            })
        })
}